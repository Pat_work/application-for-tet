// declare a module

var myAppModule=angular.module('myWeatherApp', [
  'ngRoute'
]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/weather', { templateUrl: 'views/weather.html', controller: 'weatherController' });
    $routeProvider.otherwise({ redirectTo: '/weather' });
}]);




