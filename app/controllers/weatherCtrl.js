﻿
myAppModule.controller("weatherController", function ($scope, $http) {
    var weatherUrl = "http://api.openweathermap.org/data/2.5/weather?q=";
    var apiid = "&appid=3d8b309701a13f65b660fa2c64cdc517";
    $scope.showSeaData = false;

    $http.get(weatherUrl + 'istanbul,Turkey&units=metric' + apiid)
        .then(function (res) {
            $scope.istanbuWeather = res;
            $scope.isnSunrise = (new Date(res.data.sys.sunrise * 1000)).toLocaleTimeString();
            $scope.isnSunset = (new Date(res.data.sys.sunset * 1000)).toLocaleTimeString();
        });
    $http.get(weatherUrl + 'Moscow,Russia&units=metric' + apiid)
       .then(function (res) {
           $scope.moscowWeather = res;
           $scope.mosSunrise = (new Date(res.data.sys.sunrise * 1000)).toLocaleTimeString();
           $scope.mosSunset = (new Date(res.data.sys.sunset * 1000)).toLocaleTimeString();
       });
    $http.get(weatherUrl + 'London,uk&units=metric' + apiid)
      .then(function (res) {
          $scope.londonWeather = res;
          $scope.lonSunrise = (new Date(res.data.sys.sunrise * 1000)).toLocaleTimeString();
          $scope.lonSunset = (new Date(res.data.sys.sunset * 1000)).toLocaleTimeString();
      });
    $http.get(weatherUrl + 'Berlin,Germany&units=metric' + apiid)
      .then(function (res) {
          $scope.berlinWeather = res;
          $scope.berSunrise = (new Date(res.data.sys.sunrise * 1000)).toLocaleTimeString();
          $scope.berSunset = (new Date(res.data.sys.sunset * 1000)).toLocaleTimeString();
      });
    $http.get(weatherUrl + 'Madrid,Spain&units=metric' + apiid)
      .then(function (res) {
          $scope.madridwWeather = res;
          $scope.madSunrise = (new Date(res.data.sys.sunrise * 1000)).toLocaleTimeString();
          $scope.madSunset = (new Date(res.data.sys.sunset * 1000)).toLocaleTimeString();
      });

    //&units=metric---For temperature in Celsius use units=metric


    //----------------------Sea level in the next 5 days at 9

    $scope.getSeaLevel = function (city)
    {
        $scope.seaLevelData = [];
        $scope.city_name = city;
        $http.get("http://api.openweathermap.org/data/2.5/forecast?q=" + city + apiid)
     .then(function (response) {
         
         for (var i = 0; i < response.data.list.length; i++) {
             if (new Date(response.data.list[i].dt_txt).toLocaleTimeString() == "09:00:00")
             {
                 response.data.list[i].dt_txt = new Date(response.data.list[i].dt_txt);
                 $scope.seaLevelData.push(response.data.list[i]);
                 
                 console.log($scope.seaLevelData);
                
                
             }
         }
         $scope.showSeaData = true;

     });
    };

    $scope.backtoweather = function ()
    {
        $scope.showSeaData = false;
    };



});